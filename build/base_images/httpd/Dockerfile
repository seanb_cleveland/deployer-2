# Image: agaveapi/apim_httpd
# Runs a copy of httpd with specific rewrite configs for fronting jstubbs/apim17_* images.

FROM jstubbs/template_compiler
MAINTAINER Joe Stubbs

RUN apt-get update
# RUN apt-get install -y apache2 apache2-mpm-prefork apache2-utils libexpat1 ssl-cert
RUN apt-get install -y apache2 apache2-utils libexpat1 ssl-cert

RUN apt-get install -y libapache2-mod-wsgi

RUN apt-get install -y lynx

# Manually set up the apache environment variables
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid

# base config 
RUN a2enmod rewrite
RUN a2enmod headers
RUN a2enmod proxy_http
ADD httpd.conf.j2 /etc/apache2/sites-available/000-default.conf

# SSL and certs
RUN a2enmod ssl
RUN mkdir /etc/apache2/ssl

ADD ssl/apache.crt /etc/apache2/ssl/certs/apache.crt
ADD ssl/apache.key /etc/apache2/ssl/private/apache.key
ADD ssl/ssl.conf.j2 /etc/apache2/sites-available/default-ssl.conf
RUN a2ensite default-ssl.conf

# add templates
ADD templates /templates

# add health check
RUN touch /var/www/html/apache-health-check
# add entry script
ADD entry.sh /entry.sh

# add apim public key dir
RUN mkdir -p /var/www/html/apim/v2


EXPOSE 80 443
CMD sh /entry.sh
#CMD /usr/sbin/apache2ctl -D FOREGROUND
