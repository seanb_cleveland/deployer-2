# Image: agaveapi/deployer

#from agaveapi/deployer-base-ans212
from agaveapi/deployer-base

# docker_atim
ADD deploy /deploy

# entry
ADD entry.sh /entry.sh
RUN chmod +x /entry.sh

ENTRYPOINT ["./entry.sh"]
#ENTRYPOINT ["/bin/bash"]
